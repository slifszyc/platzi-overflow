import { PlatziOverflowPage } from './app.po';

describe('platzi-overflow App', () => {
  let page: PlatziOverflowPage;

  beforeEach(() => {
    page = new PlatziOverflowPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
