import { Question } from '../models'
import { handleError } from '../utils'
import Debug from 'debug'

const debug = new Debug('platzi-overflow:question')

export async function question(req, res, next) {
  const { id } = req.params
  try {
    const question = await Question.findById(id).populate('user answers')
    if (!question) {
      debug(`Question with id ${id} not found`)
      return res.status(404).json({
        message: 'Question not found',
        error: err
      })
    }

    debug(`Question ${id} successfully found`)
    req.question = question
    next()
  } catch (err) {
    debug(`An error occurred while getting the user ${id}`)
    handleError(err, res)
  }
}
