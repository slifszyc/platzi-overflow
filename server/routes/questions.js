import express from 'express'
import { auth, user, question } from '../middlewares'
import { handleError } from '../utils'
import { Question, Answer } from '../models'

const app = express.Router()

app.get('/', async (req, res) => {
  const sort = req.query.sort || '-createdAt'
  try {
    const questions = await Question.find().populate('answers').sort(`${sort}`)
    res.status(200).json(questions)
  } catch (err) {
    handleError(err, res)
  }
})

app.get('/:id', async (req, res) => {
  try {
    const question = await Question
      .findOne({ _id: req.params.id })
      .populate('user')
      .populate({
        path: 'answers',
        options: { sort: '-createdAt' },
        populate: {
          path: 'user',
          model: 'User'
        }
      })
    res.status(200).json(question)
  } catch (err) {
    handleError(err, res)
  }
})

app.post('/', auth, user, async (req, res, next) => {
  const { title, description, icon } = req.body
  const question = new Question({
    title,
    description,
    user: req.user,
    icon
  })
  try {
    const savedQuestion = await question.save()
    req.user.questions.push(savedQuestion)
    req.user.save()
    res.status(201).json(savedQuestion)
  } catch (err) {
    console.log('hdp', err)
    handleError(err, res)
  }
})

app.post('/:id', auth, user, async (req, res, next) => {
  const { title, description, icon } = req.body
  const question = new Question({
    title,
    description,
    user: req.user,
    icon
  })
  try {
    const savedQuestion = await question.save()
    req.user.questions.push(savedQuestion)
    req.user.save()
    res.status(201).json(savedQuestion)
  } catch (err) {
    handleError(err, res)
  }
})

app.post('/:id/answers', auth, user, question, async (req, res, next) => {
  const { body: { description }, user, question } = req

  const answer = new Answer({ description, user, question})
  try {
    const savedAnswer = await answer.save()
    question.answers.push(savedAnswer)
    question.save()
    res.status(201).json(savedAnswer)
  } catch (err) {
    handleError(err, res)
  }
})

export default app
