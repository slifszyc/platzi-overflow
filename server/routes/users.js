import express from 'express'
import { User } from '../models'
import {
  hashSync as hash,
  compareSync as comparePasswords
} from 'bcryptjs'
import jwt from 'jsonwebtoken'
import { secret } from '../config'

const app = express.Router()

app.post('/signup', async (req, res, next) => {
  const user = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: hash(req.body.password, 10)
  })
  try {
    const result = await user.save()
    const token = jwt.sign({ user }, secret, { expiresIn: 86400})
    const { _id, email, firstName, lastName } = result._doc
    res.status(201).json({
      message: 'Saved user',
      token,
      userId: _id,
      firstName,
      lastName,
      email
    })
  } catch (err) {
    res.status(500).json({
      message: 'An error occurred',
      error: err
    })
  }
})

app.post('/signin', async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email })
    if (!user) {
      return handleLoginFailed(res)
    }

    if (!comparePasswords(req.body.password, user.password)) {
      return handleLoginFailed(res)
    }

    const token = jwt.sign({ user }, secret, { expiresIn: 86400})
    res.status(200).json({
      message: 'Login succeeded',
      token,
      userId: user._id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email
    })
  } catch (err) {
    res.status(500).json({
      message: 'An error occurred',
      error: err
    })
  }
})

function handleLoginFailed(res) {
  res.status(401).json({
    message: 'Login failed',
    error: `Email and password don't match`
  })
}

export default app
