import questions from './questions'
import users from './users'

export {
  questions,
  users
}
