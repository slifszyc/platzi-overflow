import { NgModule } from '@angular/core';
import {
  MdIconModule,
  MdTabsModule,
  MdToolbarModule,
  MdMenuModule,
  MdListModule,
  MdButtonModule,
  MdInputModule,
  MdRadioModule,
  MdGridListModule,
  MdCardModule,
  MdSnackBarModule,
  MdProgressSpinnerModule
} from '@angular/material';

const modules = [
  MdTabsModule,
  MdIconModule,
  MdToolbarModule,
  MdMenuModule,
  MdListModule,
  MdButtonModule,
  MdInputModule,
  MdRadioModule,
  MdGridListModule,
  MdCardModule,
  MdSnackBarModule,
  MdProgressSpinnerModule
]

@NgModule({
  imports: modules,
  exports: modules,
})
export class MaterialModule { }
