import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Material Design
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';

import { MomentModule } from 'angular2-moment';

import { AppComponent } from './app.component';
import { QuestionScreenComponent } from './question/question.screen'
import { QuestionListComponent } from './question/question-list.component'
import { QuestionFormComponent } from './question/question.form'
import { QuestionDetailComponent } from './question/question-detail.component'
import { SigninScreenComponent } from './auth/signin.screen'
import { SignupScreenComponent } from './auth/signup.screen'
import { AnswerFormComponent } from './answer/answer.form'

import { AuthService } from './auth/auth.service'

// Material Components
import { MaterialModule } from './material.module';

import { Routing } from './app.routing'

@NgModule({
  declarations: [
    AppComponent,
    QuestionScreenComponent,
    QuestionListComponent,
    QuestionFormComponent,
    QuestionDetailComponent,
    SigninScreenComponent,
    SignupScreenComponent,
    AnswerFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // Material Design
    MaterialModule,
    MomentModule,
    Routing
  ],
  providers: [ AuthService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
