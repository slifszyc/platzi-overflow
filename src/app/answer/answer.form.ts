import { Component, Input } from '@angular/core'
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router'
import { QuestionService } from '../question/question.service'
import { Answer } from './answer.model'
import { AuthService } from '../auth/auth.service'
import { Question } from '../question/question.model'
import SweetScroll from 'sweet-scroll'


@Component({
  selector: 'answer-form',
  templateUrl: './answer.form.html',
  providers: [QuestionService],
  styles: [`
    form {
      width: 90%;
      margin-top: 20px;
    }
  `]
})
export class AnswerFormComponent {

  @Input() question: Question
  sweetScroll: SweetScroll

  constructor(
    private questionService: QuestionService,
    private router: Router,
    private authService: AuthService
  ) {
    this.sweetScroll = new SweetScroll
  }

  onSubmit(form: NgForm) {
    if (!this.authService.currentUser) {
      return this.router.navigateByUrl('/signin')
    }

    const answer = new Answer(
      form.value.description,
      this.question,
      this.authService.currentUser
    )
    this.questionService.addAnswer(answer)
      .subscribe(
        (a) => {
          this.question.answers.unshift(a)
          this.sweetScroll.to('#answers-title')
          form.reset()
        },
        error => this.authService.handleError(error)
      )
    form.resetForm()
  }
}
