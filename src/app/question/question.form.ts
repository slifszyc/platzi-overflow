import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router'
import { QuestionService } from './question.service'
import { Question } from './question.model'
import { AuthService } from '../auth/auth.service'
import icons from './icons'

@Component({
  selector: 'question-form',
  templateUrl: './question.form.html',
  styleUrls: ['./question.form.css'],
  providers: [QuestionService]
})
export class QuestionFormComponent implements OnInit {
  icons: Array<Object> = icons

  constructor(
    private questionService: QuestionService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    if (!this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/signin')
    }
  }

  onSubmit(form: NgForm) {
    const q = new Question(
      form.value.title,
      form.value.description,
      null,
      form.value.icon
    )
    this.questionService.addQuestion(q)
      .subscribe(
        ({ _id }) => this.router.navigate(['/questions', _id]),
        this.authService.handleError
      )
    form.resetForm()
  }
}
